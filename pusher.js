$(function(){
    pusher.init();
})
var pusher = {
    socket : null,
    user_id : 0,
    init : function(){
        var self = this;
        self.user_id = $CONFIG.user_id ? $CONFIG.user_id : 0;
        if(self.user_id <= 0){
            return false;
        }
        self.socket = io.connect('ws://localhost:3000');
        self.socket.emit('login', {userid:this.user_id});
        self.socket.on("message",function(rep){
            console.log("接收信息:"+rep);
        })
    }
}